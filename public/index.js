const names = ["IoT Developer", "Researcher", "Software Engineer"];
let speed = 50;
let index = 0;

function typeWriter() {
  let text = names[index];
  let i = 0;

  function type() {
    if (i < text.length) {
      document.getElementsByClassName("sub-heading")[0].innerHTML +=
        text.charAt(i);
      i++;
      setTimeout(type, speed);
    } else {
      setTimeout(clearText, 1000);
    }
  }

  function clearText() {
    let currentText =
      document.getElementsByClassName("sub-heading")[0].innerHTML;
    let eraseInterval = setInterval(() => {
      if (currentText.length > 0) {
        currentText = currentText.slice(0, -1);
        document.getElementsByClassName("sub-heading")[0].innerHTML =
          currentText + "";
      } else {
        clearInterval(eraseInterval);
        index = (index + 1) % names.length;
        setTimeout(typeWriter, 500);
      }
    }, speed);
  }
  type();
}
typeWriter();

// Smooth scrolling
document.querySelectorAll(".floating-nav a").forEach((anchor) => {
  anchor.addEventListener("click", function (e) {
    e.preventDefault();

    document.querySelector(this.getAttribute("href")).scrollIntoView({
      behavior: "smooth",
    });
  });
});
